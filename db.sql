CREATE SCHEMA rsa ;
CREATE TABLE rsa.car_info
(
    id bigserial primary key,
    mark_and_model varchar(150),
    vin varchar(17),
    plate varchar(50),
    frame varchar(30),
    other_info text,
    passengers smallint,
    max_weight_allowed int,
    powerhp real
);
COMMENT ON COLUMN rsa.car_info.max_weight_allowed
    IS 'максимально допустимый вес';

CREATE TABLE rsa.input_data
(
    id bigserial primary key,
    value text,
    type smallint,
    status smallint
);
COMMENT ON COLUMN rsa.input_data.type
    IS '1 - VIN
2 - Госномер
3  - Номер рамы';
COMMENT ON COLUMN rsa.input_data.status
    IS '0 - новый
1 - обработан
2 - ошибка';

CREATE TABLE rsa.logs
(
    id bigserial primary key,
    input_data_id bigint,
    event_datetime timestamp without time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
    message text,
    type integer
);
COMMENT ON COLUMN rsa.logs.input_data_id
    IS 'связь с таблицей input_data
0 - значит системное';

COMMENT ON COLUMN rsa.logs.type
    IS '0 - Success
10 - Captcha service error
11  - Dkmb error
12 - Input data error
15 - Unknown error (db, network and etc)'
;

CREATE TABLE rsa.insurances
(
    id bigserial primary key,
    "number" varchar(50),
    insurance_company varchar(200),
    active boolean,
    car_id bigint,
    insurance_expire varchar(150),
    vehicle_follow_to_registration boolean,
    vehicle_with_trailer boolean,
    vehicle_target varchar(150),
    who_can_use varchar(150),
    insurancer varchar(150),
    vehicle_owner varchar(150),
    kbm real,
    insurance_region varchar(150),
    insurance_premium real,
    insurance_premium_currency varchar(50),
    request_date date
)
;

COMMENT ON COLUMN rsa.insurances.car_id
    IS 'связь с таблицей car_info';

COMMENT ON COLUMN rsa.insurances.kbm
    IS '0 если сведения отсутсвуют';

COMMENT ON COLUMN rsa.insurances.insurance_premium
    IS '0 если сведения отсутсвуют';
