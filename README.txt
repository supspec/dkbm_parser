1. Установка парсера (Unix).
    1. Для установки парсера в Unix среду, требуется установленный python3.7, python3-venv и git

    2. В примере установка происходит в директорию /usr/local (можно устанавливать
    в любую дректорию, права root не нужны)

    3. Скачиваем дистрибутив:
    cd /usr/local
    git clone https://supspec@bitbucket.org/supspec/dkbm_parser.git

    4. Создание виртуального окружения для парсера:
    cd ./dkbm_parser
    python3.7 -m venv venv

    4.1 и его активация
    source venv/bin/activate

    4.2 дективация виртуального окружения
    deactivate

    5. Установка зависимостей (виртаульное окружение должно быть активирована)
    pip install -r requiremets.txt

    5. Далее необходимо скопировать и отредактировать файл конфига (описание
    конфига приведено далее):
    cp config.sample.py config.py

    6. Создать структуру бд из файла db.sql,
    (пример создания из под юзера postres)
    psql postgres
    \c dkbm
    \i /usr/local/dkbm_parser/db.sql


    7. Загрузите данные для парсинга в таблицу input_data любым удобным для вас
    способом (пример в файле example_input_data.sql)

    Запуск с активированной виртуальной средой:
    cd /usr/local/dkbm_parser && python parser.py

    Запуск с неактивной средой (например из крона):
    /usr/local/dkbm_parser/venv/bin/python /usr/local/dkbm_parser/parser.py

2. 1. Установка парсера (Windows).
    1. Для установки парсера в Windows среду, требуется установленный python3.7,
    git и postgresql

    https://www.python.org/ftp/python/3.7.9/python-3.7.9-amd64.exe
    https://git-scm.com/download/win
    https://www.enterprisedb.com/postgresql-tutorial-resources-training?cid=48



    2. Откройте cmd и перейдите в C: (можно в любое другое место)
        cd \
    3. Скайте парсер:

        git clone https://supspec@bitbucket.org/supspec/dkbm_parser.git

        cd dkbm_parser

    4. Создание виртуального окружения и его активация
        python -m venv venv
        venv\Scripts\activate
    5. Установка зависимостей:
        pip install -r requirements-win.txt


    6. Создание базы
        6.1 Вместе с устнвленным postgresql у вас должны поставиться пакеты
        (pgadmin4 и psql), запустите psql (из меню Start), введите Server,
        Database, Port, Username и Password (который вы указали на этапе
        установки postgresql)
        6.2 В запущенном psql создайте базу
            create database dkbm;
        6.3. Зайдите в базу:
            \c dkbm
        6.4.  Выполните скрипт, создающий структуру базы:
            \i C:/dkbm_parser/db.sql

         6.5 Можете также загрузить в базу данные для парсинга из файла
         example_input_data.sql
         \i C:/dkbm_parser/example_input_data.sql

     7. Скопируйте config.sample.py в config.py и отредактируйте его (см.
     описание ниже)

     8. Запускайте с активированной виртаульной средой
        python parser.py

3. Файл конфига (config.py)

# Параметры подкчлюения к бд
db_host = '127.0.0.1'
db_user = 'postgres'
db_password = '123qwe'
db_name = 'dkbm'
db_port = 5432
db_schema = 'rsa'

parse_invalid = False # парсить ВИНы, которые ранее спарсились с ошибкой

log_file = 'log.txt' # файл с логами, в режиме debug = True сюда пишутся
сообщения о ходе выполения скрипта, в режиме debug = False - сообщения об
ошибках (например проблемы с подключением к бд)

captcha_key = '' # ключ для captcha.guru
recaptcha_site_key = '6Lf2uycUAAAAALo3u8D10FqNuSpUvUXlfP7BzHOk' # ключ
рекапчи (находится на странице с капчей, менять не должен)

threads = 50 # количество потоков
try_before_skip = 10 # Сколько делать попыток получить результат, менять не
стоит

proxy_file = 'proxy.txt' # путь до файла проксей, если proxy_enabled = True
proxy_enabled = False # включить прокси
debug = False # Многословные логи или только ошибки (имеется ввиду логи в файле log_file)


Прокси в файле proxy.txt должны быть в фоормате:

http://username:password@proxy_ip1:proxy_port
http://username:password@proxy_ip2:proxy_port

p.s. Ресурс не является требовательным к проксям (в тестовом режиме парсер
около часа работал в 50 потоков с 3 проксями и не был забанен), те не менее, в многопоточном
режиме рекомендуется иметь хотя бы по 1 проксе на 2-3 потока (можно брать
Shared proxy с https://proxy.house)