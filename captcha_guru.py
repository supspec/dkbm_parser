import requests
import time
import logging
from exceptions import DkbmParseException


class CaptchaGuru:
    def __init__(self, captcha_guru_key, logger: logging.Logger,
                 captcha_try=20,
                 sleep_between=10):
        self._captcha_guru_key = captcha_guru_key
        self._captcha_try = captcha_try
        self._sleep_between = sleep_between
        self._logger = logger

    @property
    def captcha_guru_key(self):
        return self._captcha_guru_key

    @captcha_guru_key.setter
    def captcha_guru_key(self, captcha_guru_key):
        self._captcha_guru_key = captcha_guru_key

    def recognize(self, site_key, page_url):
        '''Распознаем'''
        captcha_id = self._send_captcha(site_key, page_url)
        if captcha_id:
            for _ in range(self._captcha_try):
                time.sleep(self._sleep_between)
                res = self._get_captcha(captcha_id)
                if res.startswith('OK|'):
                    # self._logger.info(f"Captcha result: {res}")
                    return res.split('|')[1]
                self._logger.info(res)
                if 'ERROR_CAPTCHA_UNSOLVABLE' in res:
                    break

        raise DkbmParseException(code=10, message=f"Can't recognize captcha "
                                                  f"with captcha_id: "
                                                  f"{captcha_id}| {res}")
        return False

    def _get_captcha(self, id):
        return requests.get(
            f'http://api.captcha.guru/res.php?key={self._captcha_guru_key}&action=get&id={id}').content.decode()

    def _send_captcha(self, site_key, page):
        content = requests.get(
            f'http://api.captcha.guru/in.php?key={self._captcha_guru_key}&method=userrecaptcha&googlekey={site_key}&pageurl={page}').content.decode()
        p = content.split('|')
        if p[0] == 'OK':
            self._logger.info(f"Sended captcha: {p[1]}")
            return p[1]
        raise DkbmParseException(code=10, input_data_id=0, message=f"Can't "
                                                                   f"send "
                                                                   f"captcha")
        return False
