import config as cfg
from db import Database
import logging
from captcha_guru import CaptchaGuru
import requests
import time
import lxml
from multiprocessing.pool import ThreadPool
from bs4 import BeautifulSoup
from datetime import datetime
import urllib3
import pytz
from proxy import Proxy as p
from exceptions import DkbmParseException

def isDigit(x):
    try:
        float(x)
        return True
    except ValueError:
        return False

class Dkbm:
    '''Класс содержит статические методы, для получения и обработки
    результатов парсинга'''

    @staticmethod
    def parse_page(html_source, request_date):
        '''Парсит готовый html - получает информацию о транспортном средстве
        и его
        полисах'''
        soup = BeautifulSoup(html_source, 'lxml')
        polices = soup.find('table', class_='policies-tbl')
        trs = polices.find_all('tr', class_='data-row')
        car_info = {}
        car_info['mark_and_model'] = ''
        car_info['vin'] = ''
        car_info['plate'] = ''
        car_info['chassie'] = ''
        car_info['frame'] = ''
        car_info['other_info'] = ''
        car_info['passengers'] = 0
        car_info['max_weight_allowed'] = 0
        car_info['powerhp'] = 0
        insurances = []
        if len(trs):
            for tr in trs:
                cols = tr.find_all('td')
                number = cols[1].text
                insurance_company = cols[2].text
                active = cols[3].text
                insurance_expire = cols[4].text
                insurance_premium = cols[-1].text
                insurance_premium = str(insurance_premium).split(" ")[0]
                # if not insurance_premium.isdigit():
                if not isDigit(insurance_premium):
                    insurance_premium = 0
                insurance_region = cols[-2].text
                kbm = cols[-3].text
                vehicle_owner = cols[-4].text
                insurancer = cols[-5].text
                who_can_use = cols[-6].text
                vehicle_target = cols[-7].text
                vehicle_with_trailer = cols[-8].text
                vehicle_follow_to_registration = cols[-9].text
                insurance_premium_currency = 'руб.'
                # insurance_premium_currency = insurance_premium.split(" ")[1]
                insurances.append({
                    'number': number,
                    'insurance_company': insurance_company,
                    'active': active == 'Действует',
                    'insurance_expire': insurance_expire,
                    'vehicle_follow_to_registration': vehicle_follow_to_registration != 'Нет',
                    'vehicle_with_trailer': vehicle_with_trailer != 'Нет',
                    'vehicle_target': vehicle_target,
                    'who_can_use': who_can_use,
                    'insurancer': insurancer,
                    'vehicle_owner': vehicle_owner,
                    'kbm': kbm if isDigit(kbm) else 0,
                    'insurance_region': insurance_region,
                    'insurance_premium': insurance_premium,
                    'insurance_premium_currency': insurance_premium_currency,
                    'request_date': request_date
                })
                vehicle_table = tr.find('table')

                for veh_tr in vehicle_table.find_all('tr'):
                    veh_cols = veh_tr.find_all('td')
                    header = veh_cols[0].text
                    value = veh_cols[1].text
                    if 'Марка и модель транспортного средства' in header:
                        car_info['mark_and_model'] = value
                        continue
                    if 'Государственный регистрационный знак' in header:
                        car_info['plate'] = value
                        continue
                    if 'VIN' in header:
                        car_info['vin'] = "отсутствует" if len(value) > 17 else value
                        continue
                    if 'Номер кузова' in header:
                        car_info['frame'] = value
                        continue
                    if 'Номер шасси' in header:
                        car_info['chassie'] = value
                        continue
                    if 'Мощность двигателя для категории' in header:
                        car_info['powerhp'] = float(value)
                        continue
                    if 'Максимальная разрешенная масса' in header:
                        car_info['max_weight_allowed'] = int(value)
                        continue
                    car_info['other_info'] = f"{car_info['other_info']}" \
                                             f"{header}: {value}\n"
            return {
                'car_info': car_info,
                'insurances': insurances
            }
        return False

    @staticmethod
    def get_info(captcha_guru: CaptchaGuru, request_date,
                 vin='', chassisNumber='', bodyNumber='', licensePlate=''):
        '''Получить html страницы с результатами проверки страховок. Можно
        передавать вин, номер шасси, номер кузова или гос номер'''
        session = requests.Session()

        proxy = None
        if cfg.proxy_enabled:
            proxy = p.get_proxy()

        session.headers[
            'User-Agent'] = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) ' \
                            'AppleWebKit/537.36 (KHTML, like Gecko) ' \
                            'Chrome/85.0.4183.83 Safari/537.36'
        session.get('https://dkbm-web.autoins.ru/dkbm-web-1.0/policyInfo.htm',
                    verify=False, proxies=proxy)
        captcha = captcha_guru.recognize(cfg.recaptcha_site_key,
                                         'https://dkbm-web.autoins.ru/dkbm-web-1.0/policyInfo.htm')
        if captcha:
            # logger.info(f"Captcha res: {captcha}")
            data = {
                'bsoseries': 'CCC',
                'bsonumber': '',
                'requestDate': request_date,
                'vin': vin,
                'licensePlate': licensePlate,
                'bodyNumber': bodyNumber,
                'chassisNumber': chassisNumber,
                'isBsoRequest': False,
                'captcha': captcha
            }
            resp_policyInfo = session.post(
                'https://dkbm-web.autoins.ru/dkbm-web-1.0/policyInfo.htm',
                data=data, verify=False, proxies=proxy)
            response_data = resp_policyInfo.json()
            # logger.info(f"{response_data}")
            if response_data['validCaptcha'] and not response_data[
                'errorMessage']:
                processId = response_data['processId']
                data['processId'] = processId
                data['g-recaptcha-response'] = ''
                # logger.info(data)
                resp = session.post(
                    'https://dkbm-web.autoins.ru/dkbm-web-1.0/policyInfoData.htm',
                    data=data,
                    verify=False, proxies=proxy)
                content = resp.content.decode()
                # logger.info(content)
                if 'Сведения о договоре ОСАГО' in content:
                    return content
        return False

    @staticmethod
    def excute_parsing(row, captcha, logger):
        '''Единый метод для работы с данным классом. Делает запрос и
        возвращает распаршенный результат. Передаем словарь из input_data,
        а также объекты капчи и логгера'''
        html = ''
        request_date = datetime.now(pytz.timezone('Europe/Moscow')).strftime(
            "%d.%m.%Y")
        request_date = row['request_date'].strftime("%d.%m.%Y") if row[
            'request_date'] else request_date
        for _ in range(cfg.try_before_skip):
            if row['type'] in (1, 2, 3, 4):
                try:
                    if row['type'] == 1:
                        html = Dkbm.get_info(captcha, request_date, vin=row['value'])
                    elif row['type'] == 4:
                        html = Dkbm.get_info(captcha, request_date, bodyNumber=row['value'])
                    elif row['type'] == 3:
                        html = Dkbm.get_info(captcha, request_date, chassisNumber=row['value'])
                    elif row['type'] == 2:
                        html = Dkbm.get_info(captcha, request_date, licensePlate=row['value'])
                except DkbmParseException as err:
                    logger.info(f"Can't get info for {row}, try again!")
                if html:
                    break
            else:
                raise DkbmParseException(code=12,
                                         input_data_id=row['id'],
                                         message=f"Invalid input_data type, {row}")
        if html:
            try:
                results = Dkbm.parse_page(html_source=html, request_date=request_date)
            except AttributeError as err:
                raise DkbmParseException(input_data_id=row['id'],
                                         code=11, message=f"Не найдено "
                                                          f"полисов для "
                                                          f"{row['value']}")

            if not results:
                raise DkbmParseException(input_data_id=row['id'],
                                         code=11, message=f"Bad results")
            return results
        else:
            raise DkbmParseException(code=11,
                                     input_data_id=row['id'],
                                     message=f"Empty results for {row}")

    @staticmethod
    def wrapper(params):
        row, captcha, logger = params
        try:
            return {
                'results': Dkbm.excute_parsing(row, captcha, logger),
                'input_id': row['id']
            }
        except DkbmParseException as err:
            return {
                'input_id': row['id'],
                'results': {
                    'errorCode': err.code,
                    'error': err.message,
                }}
        except Exception as err:
            return {
                'input_id': row['id'],
                'results': {
                    'errorCode': 15,
                    'error': err,
                }}


class DkbmDB:
    '''Обертка для работы с базой Dkbm'''

    def __init__(self, db: Database):
        self._db = db

    def _save_to_log(self, input_date_id, message, type):
        self._db.insert_row_and_return_last_id(
            "INSERT INTO logs (input_data_id, message, type) "
            "VALUES (%s, %s, %s)", data=(input_date_id, str(message), type))

    def _update_input_data(self, input_data_id, status):
        '''Обновить статус винов, 1 - обработан, 2 - ошибка'''
        self._db.update_rows("UPDATE input_data SET status = %s WHERE id = "
                             "%s", data=(status, input_data_id))

    def _save_polices(self, car_info_id, polices):
        '''Сохранить полисы, вернет список id из таблицы insurances'''
        polices_ids = []
        for police in polices:
            polices_ids.append(self._db.insert_row_and_return_last_id(
                "INSERT INTO insurances (number, insurance_company, "
                "active, car_id, insurance_expire, "
                "vehicle_follow_to_registration, vehicle_with_trailer, "
                "vehicle_target, who_can_use, insurancer, vehicle_owner, kbm, "
                "insurance_region, insurance_premium, "
                "insurance_premium_currency, request_date) VALUES "
                "(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, "
                "%s, %s, %s)",
                data=(police['number'], police['insurance_company'],
                      police['active'], car_info_id,
                      police['insurance_expire'],
                      police['vehicle_follow_to_registration'],
                      police['vehicle_with_trailer'],
                      police['vehicle_target'], police['who_can_use'],
                      police['insurancer'], police['vehicle_owner'],
                      police['kbm'], police['insurance_region'],
                      police['insurance_premium'],
                      police['insurance_premium_currency'],
                      police['request_date'])))
        return polices_ids

    def _save_car_info(self, car_info):
        '''Сохранить информацию об, авто, если запись уже существует,
        то вернет ее id, если записи нет, то сохранит ее и вернет новый id'''
        logger.info(f"{car_info}")
        car_info_id = False
        if car_info['vin'] != 'отсутствует':
            car_info_id = self._get_car_by_vin(car_info['vin'])
        else:
            car_info_id = self._get_car_by_plate(car_info['plate'])
        if car_info_id:
            return car_info_id
        return self._db.insert_row_and_return_last_id(
            "INSERT INTO car_info (mark_and_model, vin, plate, frame, "
            "other_info, passengers, max_weight_allowed, powerhp) VALUES "
            "(%s, %s, %s, %s, %s, %s, %s, %s)", data=(
                car_info['mark_and_model'], car_info['vin'], car_info['plate'],
                car_info['frame'], car_info['other_info'],
                car_info['passengers'],
                car_info['max_weight_allowed'], car_info['powerhp']
            ))

    def _get_car_by_vin(self, vin):
        '''Вернуть id авто, по VIN, если нет записи - вернет False'''
        res = self._db.select_rows_dict_cursor("SELECT id FROM car_info WHERE "
                                               "vin = %s", data=(vin,))
        if res:
            return res[0]['id']
        return False

    def _get_car_by_plate(self, plate):
        '''Вернуть id авто, по Номеру, если нет записи - вернет False'''
        res = self._db.select_rows_dict_cursor("SELECT id FROM car_info WHERE "
                                               "plate = %s", data=(plate,))
        if res:
            return res[0]['id']
        return False

    def save_car_parse_resuls(self, input_id, results):
        '''Сохранить результаты парсинга, если произошла ошибка, то нужно
        передать словарь results['errorCode'] и results['error']'''
        try:
            if 'errorCode' in results:
                self._save_to_log(input_id, results['error'],
                                  results['errorCode'])
                self._update_input_data(input_id, 2)
            else:
                car_info_id = self._save_car_info(results['car_info'])
                polices = self._save_polices(car_info_id,
                                             results['insurances'])
                self._save_to_log(input_id, f"Добавлены полисы {polices}, "
                                            f"для car_id = {car_info_id}", 0)
                self._update_input_data(input_id, 1)
        except Exception as err:
            # logger.error()
            raise Exception(f"{input_id} {results} Error: {str(err)}")


class DkbmParse:

    def __init__(self, db: Database, logger: logging.Logger):
        self._db = db
        self._logger = logger
        self._init_captcha()
        if cfg.proxy_enabled:
            self._logger.info(f"Proxy Enabled.")

    def _init_captcha(self):
        self._captcha = CaptchaGuru(cfg.captcha_key, self._logger, 30, 5)
        self._logger.info(f"Init captcha object!")

    def load_input_data(self):
        sql = "SELECT id, value, type, request_date FROM input_data WHERE status = 0"
        if cfg.parse_invalid:
            '''Если перепроверяем ошибочные'''
            sql = "SELECT id, value, type, request_date FROM input_data WHERE status IN (" \
                  "0, 2)"
        self.input_data = self._db.select_rows_dict_cursor(sql)
        self._logger.info(f"Find {len(self.input_data)} elements for parsing!")

    def run(self):
        dkdb = DkbmDB(self._db)
        input_rows = []
        for row in self.input_data:
            input_rows.append((row, self._captcha, self._logger))

        pool = ThreadPool(cfg.threads)
        imap_unordered_it = pool.imap_unordered(Dkbm.wrapper, input_rows)
        for i, results in enumerate(imap_unordered_it):
            dkdb.save_car_parse_resuls(results['input_id'], results['results'])
            print(f'parsed {i + 1} from {len(input_rows)} ')
        pool.close()
        pool.join()
        # self._logger.info(f"{row}")

        # break


if __name__ == '__main__':
    urllib3.disable_warnings()
    logger = logging.getLogger(__name__)
    f_handler = logging.FileHandler(cfg.log_file, 'a+', 'utf-8')
    f_handler.setFormatter(logging.Formatter(
        '%(asctime)s - %(name)s - %(levelname)s - %(message)s'))
    f_handler.setLevel(logging.INFO)
    logger.addHandler(f_handler)
    if cfg.debug:
        logger.setLevel(logging.INFO)
    else:
        logger.setLevel(logging.WARNING)

    try:
        db = Database(cfg.db_host, cfg.db_user, cfg.db_password, cfg.db_name,
                      cfg.db_schema, cfg.db_port, logger=logger)
    except Exception as err:
        logger.exception(f"{str(err)}")
        exit()

    try:
        parser = DkbmParse(db, logger)
        parser.load_input_data()
        parser.run()
    except Exception as err:
        dk = DkbmDB(db)
        dk._save_to_log(0, f"{err}", 15)
        logger.exception(f"{err}")
