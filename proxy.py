import config as cfg
import os
from random import choice
from exceptions import DkbmParseException


class Proxy:
    @staticmethod
    def get_proxy():
        if cfg.proxy_enabled and cfg.proxy_file:
            if os.path.exists(cfg.proxy_file):
                with open(cfg.proxy_file) as fd:
                    proxyList = [proxyline.strip() for proxyline in fd.readlines() if len(proxyline.strip()) > 6]
                    proxy = choice(proxyList)
                    # print(proxy)
                    return {
                        'http': proxy,
                        'https': proxy
                    }
            else:
                raise Exception(f"Proxy file not found {cfg.proxy_file}")
        return None