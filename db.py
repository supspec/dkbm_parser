import psycopg2
from psycopg2.extras import DictCursor
import logging



class Database:
    """PostgreSQL Database class."""

    def __init__(self, host, username, password, database, db_schema, port,
                 logger: logging.Logger):
        self.host = host
        self.username = username
        self.password = password
        self.port = port
        self.dbname = database
        self.db_schema = db_schema
        self.logger = logger
        self.conn = None

    def connect(self):
        """Connect to a Postgres database."""
        if self.conn is None:
            try:
                self.conn = psycopg2.connect(
                    host=self.host,
                    user=self.username,
                    password=self.password,
                    port=self.port,
                    dbname=self.dbname,
                    options=f"-c search_path={self.db_schema}"
                )
                self.conn.set_client_encoding('UTF8')
            except psycopg2.DatabaseError as e:
                self.logger.error(e)
                raise e
            finally:
                self.logger.info('DB connection opened successfully.')

    def select_rows(self, query, data=()):
        """Run a SQL query to select rows from table."""
        self.connect()
        with self.conn.cursor() as cur:
            cur.execute(query, data)
            records = [row for row in cur.fetchall()]
            cur.close()
            return records

    def select_rows_dict_cursor(self, query, data=()):
        """Run SELECT query and return dictionaries."""
        self.connect()
        with self.conn.cursor(cursor_factory=DictCursor) as cur:
            cur.execute(query, data)
            records = [dict(row) for row in cur.fetchall()]
            # records = cur.fetchall()
            # if records:
            #     records = [dict(row) for row in records]
        cur.close()
        return records

    def update_rows(self, query, data=()):
        """Run a SQL query to update rows in table."""
        self.connect()
        with self.conn.cursor() as cur:
            cur.execute(query, data)
            self.conn.commit()
            cur.close()
            self.logger.info(f"{cur.rowcount} rows affected.")
            return f"{cur.rowcount} rows affected."

    def insert_row_and_return_last_id(self, query, data=()):
        """Run a SQL query to insert row in table and return inserted id."""
        if not str(query).lower().replace(' ', '').endswith('returningid'):
            query = query + " RETURNING id"
        self.connect()
        with self.conn.cursor() as cur:
            cur.execute(query, data)
            self.conn.commit()
            inserted_id = cur.fetchone()[0]
            cur.close()
            self.logger.info(f"inserted with id = {inserted_id}")
            return inserted_id
