db_host = '127.0.0.1'
db_user = 'postgres'
db_password = '123qwe'
db_name = 'dkbm'
db_port = 5432
db_schema = 'rsa'

parse_invalid = False # парсить ВИНы, которые ранее спарсились с ошибкой

log_file = 'log.txt'

captcha_key = '' # ключ для captcha.guru
recaptcha_site_key = '6Lf2uycUAAAAALo3u8D10FqNuSpUvUXlfP7BzHOk' # ключ
# рекапчи (находится на странице с капчей, менять не должен)
threads = 50
try_before_skip = 10 # Сколько делать попыток получить результат

proxy_file = 'proxy.txt'
proxy_enabled = False
debug = False # Многословные логи или только ошибки (имеется ввиду логи в
# файле log_file)