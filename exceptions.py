class DkbmParseException(Exception):
    def __init__(self, code, message, input_data_id = 0):
        self.code = code
        self.message = message
        self.input_data_id = input_data_id

    def __str__(self):
        return f"Input data: {self.input_data_id}, Error code: {self.code}, Message:" \
               f" {self.message}"

    def __repr__(self):
        return f"DkbmParseException(code='{self.code}', message='" \
               f"{self.message}', input_data_id = {self.input_data_id})"